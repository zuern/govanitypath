// Copyright 2023 Kevin Zuern. All rights reserved.

package main

import (
	"embed"
	"fmt"
	"html/template"
)

var (
	//go:embed tmpl/*.gotmpl
	tmplDir embed.FS

	indexTemplate  *template.Template
	moduleTemplate *template.Template
)

const (
	indexTemplateName  = "index.gotmpl"
	moduleTemplateName = "module.gotmpl"
)

func init() {
	templates := template.Must(template.ParseFS(tmplDir, "tmpl/*.gotmpl"))
	indexTemplate = mustLoadTemplate(templates, indexTemplateName)
	moduleTemplate = mustLoadTemplate(templates, moduleTemplateName)
}

func mustLoadTemplate(templates *template.Template, name string) *template.Template {
	tmpl := templates.Lookup(name)
	if tmpl == nil {
		panic(fmt.Sprintf("Could not find template tmpl/%s", name))
	}
	return tmpl
}

type templateModel struct {
	AppName    string
	AppVersion string
	Data       any
}

func newTemplateModel(data any) *templateModel {
	return &templateModel{
		AppName:    appName,
		AppVersion: appVersion,
		Data:       data,
	}
}
