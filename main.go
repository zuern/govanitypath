// Copyright 2023 Kevin Zuern. All rights reserved.

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"

	"zuern.dev/cli"
)

const (
	appName    = "govanitypath"
	appVersion = "0.0.1"
	appUsage   = appName + "[opts...] <config>"
	appDesc    = "" +
		"Creates a web server which serves webpages used by Go command to " +
		"resolve remote import paths. See " +
		"https://pkg.go.dev/cmd/go#hdr-Remote_import_paths"
)

func main() {
	var (
		port       = 8080
		configPath string
	)
	cli.Create(
		appName,
		appDesc,
		appUsage,
		appVersion,
		[]cli.Opt{
			{Name: "port", Desc: "HTTP port to serve on", Env: "HTTP_PORT", Ptr: &port},
		},
		[]cli.Arg{
			{Name: "config", Desc: "Path to JSON config file", Ptr: &configPath},
		},
	)

	log := func(format string, v ...interface{}) {
		fmt.Fprintln(os.Stderr, fmt.Sprintf(format, v...))
	}

	configFile, err := os.Open(configPath)
	if err != nil {
		log("read config %q: %s", configPath, err.Error())
		os.Exit(1)
	}
	if err := Main(port, configFile, log); err != nil {
		log("[ERR]: %s", err)
		os.Exit(1)
	}
}

func Main(
	port int,
	configFile io.ReadCloser,
	log func(fmt string, v ...interface{}),
) (err error) {
	var config Config
	if err = json.NewDecoder(configFile).Decode(&config); err != nil {
		configFile.Close()
		return fmt.Errorf("unmarshalling config from json failed: %w", err)
	}
	configFile.Close()
	server := newServer(config, log)
	listener, err := net.Listen("tcp4", fmt.Sprintf("0.0.0.0:%d", port))
	if err != nil {
		return err
	}
	log("Listening on %s", listener.Addr())
	err = http.Serve(listener, server)
	if errors.Is(err, http.ErrServerClosed) {
		return nil
	}
	return err
}
