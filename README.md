# Go Vanity Path

This is a web server which provides `<meta>` tags in its pages to tell the `go`
tool where to locate source code identified by a custom name.

For details read:
- https://pkg.go.dev/cmd/go#hdr-Remote_import_paths


## TODO
- Implement automatic source links for GitLab and GitHub as per
  https://github.com/golang/gddo/wiki/Source-Code-Links
- Tests
