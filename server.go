// Copyright 2023 Kevin Zuern. All rights reserved.

package main

import (
	"net/http"
	"strings"
)

type server struct {
	cfg Config
	mux *http.ServeMux
	log logFunc
}

func newServer(cfg Config, log logFunc) *server {
	s := &server{
		cfg: cfg,
		mux: http.NewServeMux(),
		log: log,
	}
	s.routes()
	return s
}

func (s *server) routes() {
	handle := func(path string, handlerFunc http.HandlerFunc) {
		s.mux.HandleFunc(path, handlerFunc)
		s.log("Registered route: GET %s%s", s.cfg.Domain, path)
	}
	handle("/", s.HandleHomepage())
	for _, module := range s.cfg.Modules {
		handle("/"+module.Name, s.HandleModule(module))
	}
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.mux.ServeHTTP(w, r)
}

func (s *server) HandleHomepage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if len(r.URL.Path) > 2 {
			// Only serve "/" with this handler.
			http.NotFound(w, r)
			return
		}
		data := newTemplateModel(s.cfg)
		if err := indexTemplate.Execute(w, data); err != nil {
			s.log("[ERR] Executing index template with data %+v: %w", data, err)
		}
	}
}

func (s *server) HandleModule(mod Module) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		path := strings.TrimPrefix(strings.TrimSuffix(r.URL.Path, "/"), "/")
		var mod *Module
		for _, m := range s.cfg.Modules {
			if m.Name == path {
				mod = &m
				break
			}
		}
		if mod == nil {
			s.log("[ERR] Handling request GET %s but could not find a matching module from config", r.URL.Path)
			http.Error(w, http.StatusText(500), 500)
			return
		}

		moduleName := s.cfg.Domain + "/" + mod.Name
		model := moduleTemplateModel{
			ModuleName: moduleName,
			Module:     *mod,
		}
		if strings.HasPrefix(mod.URL, "https://github.com") {
			model.Source = githubSource(mod.URL)
		} else if strings.HasPrefix(mod.URL, "https://gitlab.com") {
			model.Source = gitlabSource(mod.URL)
		}
		data := newTemplateModel(model)
		if err := moduleTemplate.Execute(w, data); err != nil {
			s.log("[ERR] Executing module template with data %+v: %w", data, err)
		}
	}
}
