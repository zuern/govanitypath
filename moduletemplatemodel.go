// Copyright 2023 Kevin Zuern. All rights reserved.

package main

import "fmt"

type moduleTemplateModel struct {
	ModuleName string
	Module     Module
	Source     source
}

type source struct {
	Home      string
	Directory string
	File      string
}

const defaultGitBranch = "main"

func githubSource(moduleURL string) source {
	return source{
		Home:      moduleURL,
		Directory: fmt.Sprintf("%s/tree/%s{/dir}", moduleURL, defaultGitBranch),
		File:      fmt.Sprintf("%s/tree/%s{/dir}/{file}#L{line}", moduleURL, defaultGitBranch),
	}
}

func gitlabSource(moduleURL string) source {
	return source{
		Home:      moduleURL,
		Directory: fmt.Sprintf("%s/tree/%s{/dir}", moduleURL, defaultGitBranch),
		File:      fmt.Sprintf("%s/blob/%s{/dir}/{file}#L{line}", moduleURL, defaultGitBranch),
	}
}
