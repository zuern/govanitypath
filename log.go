// Copyright 2023 Kevin Zuern. All rights reserved.

package main

type logFunc func(format string, v ...interface{})
