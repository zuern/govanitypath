module zuern.dev/govanitypath

go 1.19

require (
	zuern.dev/cli v0.0.3
	zuern.dev/is v0.0.2
)
