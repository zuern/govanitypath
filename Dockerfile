FROM alpine as builder

RUN apk add ca-certificates tzdata && update-ca-certificates

FROM scratch

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY bin/linux/govanitypath /usr/bin/govanitypath
ENV HTTP_PORT 80
EXPOSE 80
ENTRYPOINT ["/usr/bin/govanitypath"]
