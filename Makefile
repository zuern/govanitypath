PROJECT_NAME=$(shell basename $$(git rev-parse --show-toplevel))
DOCKER_REGISTRY=registry.gitlab.com/zuern/$(PROJECT_NAME)
VERSION=$(shell TZ=UTC git show -s --format=%cd --date=format-local:%Y%m%d%H%M%S HEAD)-$(shell git describe --always --tags --dirty)

GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

## Development
build: bin/linux/$(PROJECT_NAME) ## Build the application

test: bin/codecoverage.txt ## Run tests

coverage: bin/codecoverage.txt ## Calculate code coverage
	go tool cover -func bin/codecoverage.txt

run: ## Run the application
	go run . sample_config.json

clean: ## Deletes build artifacts
	rm -rf bin

## Docker:
docker: ## Build docker image locally
	docker build . --tag $(PROJECT_NAME)

docker-push: docker ## Build and push docker images to registry
	docker tag $(PROJECT_NAME) $(DOCKER_REGISTRY)/$(PROJECT_NAME):latest
	docker tag $(PROJECT_NAME) $(DOCKER_REGISTRY)/$(PROJECT_NAME):$(VERSION)
	docker push $(DOCKER_REGISTRY)/$(PROJECT_NAME):latest
	docker push $(DOCKER_REGISTRY)/$(PROJECT_NAME):$(VERSION)

## Help:
help: ## Show this help.
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}[target]${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)

bin:
	mkdir -p bin

bin/codecoverage.txt: bin *.go
	go test -cover -coverprofile bin/codecoverage.txt ./...

bin/linux/$(PROJECT_NAME): *.go
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/linux/$(PROJECT_NAME) .

.PHONY: build test coverage run docker docker-push release help
