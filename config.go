// Copyright 2023 Kevin Zuern. All rights reserved.

package main

type Config struct {
	Domain  string
	Modules []Module
}

func (c Config) Validate() error {
	// TODO:
	// - check each module is distinct name
	return nil
}

type Module struct {
	Name string
	Type ModuleType
	URL  string
}

type ModuleType string

const (
	GIT        ModuleType = "git"
	BAZAAR     ModuleType = "bzr"
	FOSSIL     ModuleType = "fossil"
	MERCURIAL  ModuleType = "hg"
	SUBVERSION ModuleType = "svn"
)
