// Copyright 2023 Kevin Zuern. All rights reserved.

package main

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"fmt"
	"net/http/httptest"
	"os"
	"testing"

	"zuern.dev/is"
)

//go:embed sample_config.json
var configBytes []byte

func TestIndexTemplate(t *testing.T) {
	is := is.New(t)
	var config Config
	is.NoErr(json.NewDecoder(bytes.NewBuffer(configBytes)).Decode(&config)) // Unmarshal sample config

	data := newTemplateModel(config)
	is.NoErr(indexTemplate.Execute(os.Stdout, data)) // Execute template
	fmt.Println()
}

func TestModuleTemplate(t *testing.T) {
	is := is.New(t)
	var config Config
	is.NoErr(json.NewDecoder(bytes.NewBuffer(configBytes)).Decode(&config)) // Unmarshal sample config

	mod := config.Modules[0]

	log := func(_ string, _ ...interface{}) {} // no-op logger

	s := newServer(config, log)
	handler := s.HandleModule(mod)

	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/"+mod.Name, nil)
	handler.ServeHTTP(rec, req)

	fmt.Println(rec.Body.String())
}
